package np.com.uashim.dithackathonhealthwelfare;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

public class RegisterHospital extends AppCompatActivity {

    private static final String TAG = RegisterHospital.class.getName();


    private Button registerBtn;
    private EditText et_name, et_email, et_password, et_phone;
    private String name,email,password,phone;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore;
    private Double lng, lat;
    private FusedLocationProviderClient flpc;
    private int locationRequestCode = 1000;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_hospital);

        flpc = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        lat = location.getLatitude();
                        lng = location.getLongitude();
                        Log.d("DATA", String.format(Locale.US, "%s -- %s", lat, lng));
                    }
                }
            }
        };

        getLocation();

        // Firebase instance
        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        // Component declaration
        registerBtn = findViewById(R.id.register_btn);
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_phone = findViewById(R.id.et_phone);


        registerBtn.setOnClickListener(v -> {
            // Extract strings
            name = et_name.getText().toString();
            email = et_email.getText().toString();
            password = et_password.getText().toString();
            phone = et_phone.getText().toString();

            MakeRegister(email, name, password, phone);
        });

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    flpc.getLastLocation().addOnSuccessListener(RegisterHospital.this, location -> {
                        if (location != null) {
                            lat = location.getLatitude();
                            lng = location.getLongitude();
                            Log.d("DATA", String.format(Locale.US, "%s - %s", lat, lng));
                        } else {
                            flpc.requestLocationUpdates(locationRequest, locationCallback, null);
                        }
                    });
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(RegisterHospital.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(RegisterHospital.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegisterHospital.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    locationRequestCode);

        } else {

            flpc.getLastLocation().addOnSuccessListener(RegisterHospital.this, location -> {
                if (location != null) {
                    lat = location.getLatitude();
                    lng = location.getLongitude();
                    Log.d("DATA", String.format(Locale.US, "%s - %s", lat, lng));
                } else {
                    flpc.requestLocationUpdates(locationRequest, locationCallback, null);
                }
            });
        }
    }

    private void MakeRegister(String email, String name, String password, String phone) {
        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(name) || TextUtils.isEmpty(phone)) {
            Toast.makeText(this, "All fields are required!", Toast.LENGTH_SHORT).show();
        } else {
            ProgressDialog pd = new ProgressDialog(this);
            pd.setCancelable(false);
            pd.setMessage("Registration on process...");
            pd.show();

            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, task -> {
                if(task.isSuccessful()) {
                    String uid = task.getResult().getUser().getUid();
                    // User data hash map
                    Map<String, Object> user = new Hashtable<>();
                    user.put("fullName", name);
                    user.put("phone", phone);
                    user.put("role", "h");
                    user.put("activate", 0);
                    user.put("lat", lat);
                    user.put("lng", lng);

                    mFirestore.collection("users").document(uid).set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            pd.cancel();
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegisterHospital.this);
                            alertDialog.setCancelable(false);
                            alertDialog.setTitle("Success");
                            alertDialog.setMessage("Your hospital has been added but you need to activate your account by personally talking to the authorities.");
                            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mAuth.signOut();
                                    finish();
                                }
                            });
                            alertDialog.show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            pd.cancel();
                            Log.e(TAG, e.getLocalizedMessage());
                        }
                    });

                } else {

                    Log.w(TAG, task.getException().getMessage());
                }
            });
        }
    }
}
