package np.com.uashim.dithackathonhealthwelfare.Interfaces.Hospital;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import np.com.uashim.dithackathonhealthwelfare.Model.User;
import np.com.uashim.dithackathonhealthwelfare.R;

public class HospitalProfile extends AppCompatActivity {

    private EditText et_name, et_phone, et_about, et_address;
    private ImageButton saveBtn;

    private FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_profile);

        et_name = findViewById(R.id.et_name);
        et_phone = findViewById(R.id.et_phone);
        et_about = findViewById(R.id.et_about);
        et_address = findViewById(R.id.et_address);
        saveBtn = findViewById(R.id.btnSave);

        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        getDatas();

        saveBtn.setOnClickListener(v -> {
            String name = et_name.getText().toString();
            String phone = et_phone.getText().toString();
            String about = et_about.getText().toString();
            String address = et_address.getText().toString();
            Map<String, Object> user = new HashMap<>();
            user.put("fullName", name);
            user.put("phone", phone);
            user.put("about", about);
            user.put("address", address);
            ProgressDialog pd = new ProgressDialog(HospitalProfile.this);
            pd.setCancelable(false);
            pd.setMessage("Saving changes...");
            pd.show();
            mFirestore.collection("users").document(mAuth.getCurrentUser().getUid()).update(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()) {
                        pd.cancel();
                        Toast.makeText(HospitalProfile.this, "Changes has been saved!", Toast.LENGTH_SHORT).show();
                    } else {
                        pd.cancel();
                        Toast.makeText(HospitalProfile.this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        });

    }

    private void getDatas() {
        mFirestore.collection("users").document(mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot doc = task.getResult();
                User hospital = doc.toObject(User.class);
                et_name.setText(hospital.getFullName());
                et_phone.setText(hospital.getPhone());
                et_about.setText(hospital.getAbout());
                et_address.setText(hospital.getAddress());
            }
        });
    }
}
