package np.com.uashim.dithackathonhealthwelfare.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import np.com.uashim.dithackathonhealthwelfare.Interfaces.Patient.SearchHospital;
import np.com.uashim.dithackathonhealthwelfare.Model.HospitalModel;
import np.com.uashim.dithackathonhealthwelfare.R;

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.CustomViewHolder> {
    List<HospitalModel> hospitalModels;
    Context ctx;
    public SearchListAdapter(SearchHospital searchHospital, List<HospitalModel> hospitalModels) {
        this.ctx = searchHospital;
        this.hospitalModels = hospitalModels;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_search, parent, false);
        return new SearchListAdapter.CustomViewHolder(view);
        
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.name.setText(hospitalModels.get(position).getFullName());
        holder.address.setText(hospitalModels.get(position).getAddress().trim());
        holder.about.setText(hospitalModels.get(position).getAbout().trim());
        holder.callBtn.setOnClickListener(v -> {
            Toast.makeText(ctx, "Calling for the help!", Toast.LENGTH_SHORT).show();
        });

    }

    @Override
    public int getItemCount() {
        return hospitalModels.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView name, address, about;
        Button callBtn;
        public CustomViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.txtHospitalName);
            address = itemView.findViewById(R.id.txtAddress);
            about = itemView.findViewById(R.id.txtAbout);
            
            callBtn = itemView.findViewById(R.id.btnCall);
        }
    }
}
