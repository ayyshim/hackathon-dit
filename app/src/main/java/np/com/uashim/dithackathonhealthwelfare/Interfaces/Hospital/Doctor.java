package np.com.uashim.dithackathonhealthwelfare.Interfaces.Hospital;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import np.com.uashim.dithackathonhealthwelfare.Adapter.DoctorListAdapter;
import np.com.uashim.dithackathonhealthwelfare.Model.DoctorModel;
import np.com.uashim.dithackathonhealthwelfare.R;

public class Doctor extends AppCompatActivity {

    // Demo Doctor Images
    private static final String doc1 = "https://firebasestorage.googleapis.com/v0/b/health-welfare-hackathon-dit.appspot.com/o/doctor1.jpg?alt=media&token=850a9eb7-4939-46b8-b5c3-9e04a1f56948";
    private static final String doc2 = "https://firebasestorage.googleapis.com/v0/b/health-welfare-hackathon-dit.appspot.com/o/doctor2.jpg?alt=media&token=fa1187d4-0eb5-4237-82f6-41152b37ac4d";
    private static final String doc3 = "https://firebasestorage.googleapis.com/v0/b/health-welfare-hackathon-dit.appspot.com/o/doctor3.jpg?alt=media&token=140deb71-67d5-4361-be0f-34d678b225ff";
    private static final String doc4 = "https://firebasestorage.googleapis.com/v0/b/health-welfare-hackathon-dit.appspot.com/o/doctor4.jpg?alt=media&token=eeec8cb1-b145-4526-8e4c-bb2c9e45b32e";

    private List<DoctorModel> docMod = new ArrayList<>();
    private RecyclerView docList;

    private FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);

        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        initDatas();

        docList = findViewById(R.id.doctorsList);


        Button add = findViewById(R.id.addDoctor);
        add.setOnClickListener(v -> {
            LayoutInflater add_form = LayoutInflater.from(this);
            final View view = add_form.inflate(R.layout.add_doctor, null);
            final AlertDialog v_dialog = new AlertDialog.Builder(this).create();
            v_dialog.setView(view);
            v_dialog.setTitle("Add a doctor : ");

            EditText et_name = view.findViewById(R.id.et_name);
            Spinner spin_specialist = view.findViewById(R.id.spin_specialist);
            EditText et_phone = view.findViewById(R.id.et_phone);
            EditText et_email = view.findViewById(R.id.et_email);
            EditText et_about = view.findViewById(R.id.et_about);
            Button btnAdd = view.findViewById(R.id.btnAdd);

            btnAdd.setOnClickListener(v1 -> {
                String name = et_name.getText().toString();
                String specialist = spin_specialist.getSelectedItem().toString();
                String phone = et_phone.getText().toString();
                String email = et_email.getText().toString();
                String about = et_about.getText().toString();

                Map<String, Object> doctor = new HashMap<>();
                doctor.put("hid", mAuth.getCurrentUser().getUid());
                doctor.put("displayImage", doc1);
                doctor.put("name", name);
                doctor.put("specialist", specialist);
                doctor.put("phone", phone);
                doctor.put("email", email);
                doctor.put("about", about);
                doctor.put("isAvailable", 1);

                if(TextUtils.isEmpty(name) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(email) || TextUtils.isEmpty(about) ) {
                    return;
                }

                btnAdd.setEnabled(false);
                v_dialog.setCancelable(false);

                mFirestore.collection("doctors").add(doctor).addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        btnAdd.setEnabled(true);
                        docMod.clear();
                        docMod = getDoctors();
                        v_dialog.cancel();
                    } else {
                        btnAdd.setEnabled(true);
                        v_dialog.setCancelable(true);
                        Toast.makeText(Doctor.this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            });
            v_dialog.show();
        });


    }

    private void initDatas() {
        mFirestore.collection("doctors").whereEqualTo("hid", mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()) {
                    QuerySnapshot doc = task.getResult();
                    for(DocumentSnapshot documentSnapshot: doc) {
                        DoctorModel doctorModel = new DoctorModel(
                                documentSnapshot.getId(),
                                documentSnapshot.getString("displayImage"),
                                documentSnapshot.getString("name"),
                                documentSnapshot.getString("specialist"),
                                documentSnapshot.getString("phone"),
                                documentSnapshot.getString("email"),
                                documentSnapshot.getString("about"),
                                Integer.parseInt(String.valueOf(documentSnapshot.get("isAvailable")))
                                );
                                docMod.add(doctorModel);
                                DoctorListAdapter doctorListAdapter = new DoctorListAdapter(Doctor.this, docMod);
                                docList.setAdapter(doctorListAdapter);
                                docList.setLayoutManager(new LinearLayoutManager(Doctor.this));

                    }
                }
            }
        });
    }

    private List<DoctorModel> getDoctors() {
        mFirestore.collection("doctors").whereEqualTo("hid", mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()) {
                    QuerySnapshot doc = task.getResult();
                    for(DocumentSnapshot documentSnapshot: doc) {
                        DoctorModel doctorModel = new DoctorModel(
                                documentSnapshot.getId(),
                                documentSnapshot.getString("displayImage"),
                                documentSnapshot.getString("name"),
                                documentSnapshot.getString("specialist"),
                                documentSnapshot.getString("phone"),
                                documentSnapshot.getString("email"),
                                documentSnapshot.getString("about"),
                                Integer.parseInt(String.valueOf(documentSnapshot.get("isAvailable")))
                        );
                        docMod.add(doctorModel);
                        DoctorListAdapter doctorListAdapter = new DoctorListAdapter(Doctor.this, docMod);
                        docList.setAdapter(doctorListAdapter);
                        docList.setLayoutManager(new LinearLayoutManager(Doctor.this));

                    }
                }
            }
        });
        return docMod;
    }
}
