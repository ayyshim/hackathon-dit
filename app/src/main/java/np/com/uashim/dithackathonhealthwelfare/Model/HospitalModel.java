package np.com.uashim.dithackathonhealthwelfare.Model;

public class HospitalModel {
    String fullName;
    String address;
    String about;

    public HospitalModel(String fullName, String address, String about) {
        this.fullName = fullName;
        this.address = address;
        this.about = about;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
