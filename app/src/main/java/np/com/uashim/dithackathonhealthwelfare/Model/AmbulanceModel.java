package np.com.uashim.dithackathonhealthwelfare.Model;

public class AmbulanceModel {
    String id;
    String type;
    String ambulance_number;
    boolean is_available;

    public AmbulanceModel(String id, String type, String ambulance_number, boolean is_available) {
        this.type = type;
        this.ambulance_number = ambulance_number;
        this.is_available = is_available;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmbulance_number() {
        return ambulance_number;
    }

    public void setAmbulance_number(String ambulance_number) {
        this.ambulance_number = ambulance_number;
    }

    public boolean isIs_available() {
        return is_available;
    }

    public void setIs_available(boolean is_available) {
        this.is_available = is_available;
    }
}
