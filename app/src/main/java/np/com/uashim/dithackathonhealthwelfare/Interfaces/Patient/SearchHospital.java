package np.com.uashim.dithackathonhealthwelfare.Interfaces.Patient;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import np.com.uashim.dithackathonhealthwelfare.Adapter.SearchListAdapter;
import np.com.uashim.dithackathonhealthwelfare.Model.HospitalModel;
import np.com.uashim.dithackathonhealthwelfare.R;
import np.com.uashim.dithackathonhealthwelfare.utils.Nearby;

public class SearchHospital extends AppCompatActivity {

    private RecyclerView searchList;
    private ProgressBar searchProgress;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore;
    private Location userLocation;
    private List<HospitalModel> hospitalModels = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_hospital);

        searchList = findViewById(R.id.searchList);
        searchProgress = findViewById(R.id.searchProgress);

        searchProgress.setVisibility(View.VISIBLE);

        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        userLocation();

    }

    private void userLocation() {
        mFirestore.collection("users").document(mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    Location location = new Location("");
                    location.setLatitude(doc.getDouble("lat"));
                    location.setLongitude(doc.getDouble("lng"));
                    userLocation = location;
                    search();
                } else {
                    userLocation = null;
                }
            }
        });
    }

    public void search() {
        mFirestore.collection("users").whereEqualTo("role", "h").whereEqualTo("activate", 1).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()) {
                    QuerySnapshot doc = task.getResult();
                    for(DocumentSnapshot documentSnapshot: doc) {
                        Location hospLoc = new Location("");
                        hospLoc.setLongitude(documentSnapshot.getDouble("lng"));
                        hospLoc.setLatitude(documentSnapshot.getDouble("lat"));

                        float distance = userLocation.distanceTo(hospLoc);
                        Log.d("Distance", ""+distance);

                        if(distance < 1000.0) {
                            Log.d("DATA", documentSnapshot.getString("fullName"));
                            mFirestore.collection("ambulances").whereEqualTo("hid", documentSnapshot.getId()).whereEqualTo("is_available", true).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if(task.isSuccessful()) {
                                        if(task.getResult().size() > 0) {
                                            Log.d("Data", "Ambulance" + task.getResult().size());
                                            mFirestore.collection("doctors").whereEqualTo("hid", documentSnapshot.getId()).whereEqualTo("isAvailable", 1).whereEqualTo("specialist", "Cardiologist").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                    if(task.isSuccessful()) {
                                                        if(task.getResult().size() > 0) {
                                                            Log.d("Data", "Doctors" + task.getResult().size());
                                                            HospitalModel hospitalModel = new HospitalModel(
                                                                    documentSnapshot.getString("fullName"),
                                                                    documentSnapshot.getString("address"),
                                                                    documentSnapshot.getString("about")
                                                            );
                                                            hospitalModels.add(hospitalModel);
                                                            searchProgress.setVisibility(View.GONE);
                                                            Log.d("Hospital", documentSnapshot.getString("fullName"));
                                                            SearchListAdapter adapter = new SearchListAdapter(SearchHospital.this, hospitalModels);
                                                            searchList.setAdapter(adapter);
                                                            searchList.setLayoutManager(new LinearLayoutManager(SearchHospital.this));
                                                        }
                                                    }
                                                }
                                            });


                                        }
                                    }
                                }
                            });
                        }

                    }
                }
            }
        });
    }

}
