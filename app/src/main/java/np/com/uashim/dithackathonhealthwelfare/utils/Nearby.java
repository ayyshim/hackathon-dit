package np.com.uashim.dithackathonhealthwelfare.utils;

import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import np.com.uashim.dithackathonhealthwelfare.Model.HospitalModel;


public class Nearby {

    public List<HospitalModel> hospitalModels = new ArrayList<>();
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firestore;
    private Location userLocation;
    private String type;
    public Nearby(FirebaseAuth firebaseAuth, FirebaseFirestore firebaseFirestore, Location userLocation, String type) {
        this.firebaseAuth = firebaseAuth;
        this.firestore = firebaseFirestore;
        this.userLocation = userLocation;
        this.type = type;
    }



}
