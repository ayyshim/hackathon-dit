package np.com.uashim.dithackathonhealthwelfare.Model;

public class DoctorModel {
    String id;
    String displayImage;
    String name;
    String specialist;
    String phone;
    String email;
    String about;
    int isAvailable;

    public DoctorModel(String id, String displayImage, String name, String specialist, String phone, String email, String about, int isAvailable) {
        this.id = id;
        this.displayImage = displayImage;
        this.name = name;
        this.specialist = specialist;
        this.phone = phone;
        this.email = email;
        this.about = about;
        this.isAvailable = isAvailable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayImage() {
        return displayImage;
    }

    public void setDisplayImage(String displayImage) {
        this.displayImage = displayImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(int isAvailable) {
        this.isAvailable = isAvailable;
    }
}
