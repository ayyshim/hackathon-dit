package np.com.uashim.dithackathonhealthwelfare.Interfaces.Patient;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import np.com.uashim.dithackathonhealthwelfare.R;

public class PatientProfile extends AppCompatActivity {

    private EditText et_name, et_age, et_phone, et_address;
    private ImageView btnSave;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_profile);

        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        et_name = findViewById(R.id.et_name);
        et_age = findViewById(R.id.et_age);
        et_phone = findViewById(R.id.et_phone);
        et_address = findViewById(R.id.et_address);
        btnSave = findViewById(R.id.btnSave);

        initData();

        btnSave.setOnClickListener(v -> {
            String name = et_name.getText().toString();
            String age = et_age.getText().toString();
            String phone = et_phone.getText().toString();
            String address = et_address.getText().toString();

            Map<String, Object> profileUpdate = new HashMap<>();
            profileUpdate.put("fullName", name);
            profileUpdate.put("age", age);
            profileUpdate.put("phone", phone);
            profileUpdate.put("address", address);
            ProgressDialog pd = new ProgressDialog(this);
            pd.setCancelable(false);
            pd.setMessage("Saving changes...");
            pd.show();
            mFirestore.collection("users").document(mAuth.getCurrentUser().getUid()).update(profileUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()) {
                        pd.cancel();
                        Toast.makeText(PatientProfile.this, "Changes has been saved!", Toast.LENGTH_SHORT).show();
                    } else {
                        pd.cancel();
                        Toast.makeText(PatientProfile.this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        });

    }

    private void initData() {
        mFirestore.collection("users").document(mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    et_name.setText(doc.getString("fullName"));
                    et_age.setText(doc.getString("age"));
                    et_address.setText(doc.getString("address"));
                    et_phone.setText(doc.getString("phone"));
                }
            }
        });
    }
}
