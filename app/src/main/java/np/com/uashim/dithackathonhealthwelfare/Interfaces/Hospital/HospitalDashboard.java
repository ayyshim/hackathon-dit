package np.com.uashim.dithackathonhealthwelfare.Interfaces.Hospital;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import np.com.uashim.dithackathonhealthwelfare.MainActivity;
import np.com.uashim.dithackathonhealthwelfare.R;

public class HospitalDashboard extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private FirebaseFirestore mFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_dashboard);


        // Firebase instances
        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        // Component Declaration
        TextView greetings = findViewById(R.id.greetings);
        Button btnAmbulance = findViewById(R.id.btnHospitalSearch);
        Button btnDoctor = findViewById(R.id.btnDoctors);
        Button btnHospitalProfile = findViewById(R.id.btnHospitalProfile);
        Button btnSignout = findViewById(R.id.btnSignout);

        greetings.setText("Welcome to Dashboard!");

        // Signout
        btnSignout.setOnClickListener(v -> {
            mAuth.signOut();
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        });

        btnAmbulance.setOnClickListener(v -> {
            Intent i = new Intent(this, Ambulance.class);
            startActivity(i);
        });

        btnDoctor.setOnClickListener(v -> {
            Intent i = new Intent(this, Doctor.class);
            startActivity(i);
        });
        btnHospitalProfile.setOnClickListener(v -> {
            Intent i = new Intent(this, HospitalProfile.class);
            startActivity(i);
        });

    }


}
