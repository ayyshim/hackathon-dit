package np.com.uashim.dithackathonhealthwelfare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import np.com.uashim.dithackathonhealthwelfare.Interfaces.Hospital.HospitalDashboard;
import np.com.uashim.dithackathonhealthwelfare.Interfaces.Patient.PatientDashboard;
import np.com.uashim.dithackathonhealthwelfare.Model.User;

public class LoginHospital extends AppCompatActivity {

    private EditText et_email, et_password;
    private Button loginBtn;
    private TextView txt_register;
    private String email, password;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_hospital);

        // Firebase instance
        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        // Component declaration
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        loginBtn = findViewById(R.id.login_btn);
        txt_register = findViewById(R.id.register_text_btn);


        // ....
        txt_register.setOnClickListener(v -> {
            Intent i = new Intent(LoginHospital.this, RegisterHospital.class);
            startActivity(i);
        });

        loginBtn.setOnClickListener(v -> {
            // Extract string
            email = et_email.getText().toString();
            password = et_password.getText().toString();

            PerformLogin(email, password);
        });

    }

    private void PerformLogin(String email, String password) {
        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "All fields are required!", Toast.LENGTH_SHORT).show();
        } else {
            ProgressDialog pd = new ProgressDialog(this);
            pd.setMessage("Login in process...");
            pd.show();
            pd.setCancelable(false);
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, task -> {
                if(task.isSuccessful()) {
                    DocumentReference documentReference = mFirestore.collection("users").document(task.getResult().getUser().getUid());
                    documentReference.get().addOnCompleteListener(task1 -> {
                        DocumentSnapshot document = task1.getResult();
                        if(document.exists()) {
                            User user = document.toObject(User.class);
                            if(user.getRole().equals("h")) {
                                if(user.getActivate() == 1) {
                                    pd.cancel();
                                    Intent i = new Intent(LoginHospital.this, HospitalDashboard.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(i);
                                    finish();
                                    Log.d("LOGINPATIENT", String.valueOf(document.getData()));   
                                } else {
                                    pd.cancel();
                                    mAuth.signOut();
                                    Toast.makeText(this, "You haven't confirmed yourself as a hospital yet. Please confirm to get access.", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                pd.cancel();
                                Toast.makeText(LoginHospital.this, "Hospital not found!", Toast.LENGTH_SHORT).show();
                                mAuth.signOut();
                            }

                        } else {
                            pd.cancel();
                            Log.d("LOGINPATIENT", "NO SUCH DOC FOUND!");
                        }
                    });

                } else {
                    pd.cancel();
                    Toast.makeText(this, "Login failed!", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("LoginPatient", e.getLocalizedMessage());
                }
            });
        }
    }
}
