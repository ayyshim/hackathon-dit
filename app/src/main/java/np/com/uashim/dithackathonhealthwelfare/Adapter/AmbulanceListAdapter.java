package np.com.uashim.dithackathonhealthwelfare.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import np.com.uashim.dithackathonhealthwelfare.Interfaces.Hospital.Ambulance;
import np.com.uashim.dithackathonhealthwelfare.Model.AmbulanceModel;
import np.com.uashim.dithackathonhealthwelfare.R;

public class AmbulanceListAdapter extends RecyclerView.Adapter<AmbulanceListAdapter.CustomViewHolder> {

    private List<AmbulanceModel> data;
    private Context ctx;
    private FirebaseFirestore mFirestore;

    public AmbulanceListAdapter(List<AmbulanceModel> ambData, Ambulance ambulance, FirebaseFirestore firestore) {

        this.data = ambData;
        this.ctx = ambulance;
        this.mFirestore =  firestore;

    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView number, type;
        Switch aSwitch;

        public CustomViewHolder(View itemView) {
            super(itemView);

            number = itemView.findViewById(R.id.ambNumber);
            type = itemView.findViewById(R.id.ambType);
            aSwitch = itemView.findViewById(R.id.avai_switch);
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_ambulances, parent, false);
        return new AmbulanceListAdapter.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.number.setText(data.get(position).getAmbulance_number());
        holder.type.setText(data.get(position).getType());
        holder.aSwitch.setChecked(data.get(position).isIs_available());

        holder.aSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Map<String, Object> ambulanceMap = new HashMap<>();
            ambulanceMap.put("is_available", isChecked);
            mFirestore.collection("ambulances").document(data.get(position).getId()).update(ambulanceMap).addOnCompleteListener(task -> {
                if(task.isSuccessful()) {
                    Toast.makeText(ctx, "Avaibility changed", Toast.LENGTH_SHORT).show();
                } else {
                    holder.aSwitch.setChecked(!isChecked);
                    Toast.makeText(ctx, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    holder.aSwitch.setChecked(!isChecked);
                    Toast.makeText(ctx, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


}
