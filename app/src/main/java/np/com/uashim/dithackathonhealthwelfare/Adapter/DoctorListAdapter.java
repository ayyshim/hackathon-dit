package np.com.uashim.dithackathonhealthwelfare.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import np.com.uashim.dithackathonhealthwelfare.Interfaces.Hospital.Doctor;
import np.com.uashim.dithackathonhealthwelfare.Model.DoctorModel;
import np.com.uashim.dithackathonhealthwelfare.R;

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.CustomViewHolder> {

    private Context ctx;
    private List<DoctorModel> data;

    public DoctorListAdapter(Doctor doctor, List<DoctorModel> data) {
        this.ctx = doctor;
        this.data = data;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        CircleImageView displayImage;
        TextView name, specialist, about;
        Button call, email;

        public CustomViewHolder(View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.displayImage);
            name = itemView.findViewById(R.id.txtName);
            specialist = itemView.findViewById(R.id.txtSpecialist);
            about = itemView.findViewById(R.id.txtAbout);
            call = itemView.findViewById(R.id.btnCall);
            email = itemView.findViewById(R.id.btnEmail);

        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_doctors, parent, false);
        return new DoctorListAdapter.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Picasso.get().load(data.get(position).getDisplayImage()).into(holder.displayImage);
        holder.name.setText("Dr. " + data.get(position).getName());
        holder.specialist.setText(data.get(position).getSpecialist());
        holder.about.setText(data.get(position).getAbout());

        if (data.get(position).getIsAvailable() == 1) {
            holder.displayImage.setBorderColor(ctx.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.displayImage.setBorderColor(ctx.getResources().getColor(R.color.secondaryBtn));
        }

        holder.displayImage.setOnClickListener(v2 -> {
            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            firebaseFirestore.collection("doctors").document(data.get(position).getId()).update("isAvailable", data.get(position).getIsAvailable() == 1 ? 0 : 1).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(ctx, "Availability changed", Toast.LENGTH_SHORT).show();
                    int currentColor = holder.displayImage.getBorderColor();
                    if(currentColor == ctx.getResources().getColor(R.color.colorPrimary)) {
                        holder.displayImage.setBorderColor(ctx.getResources().getColor(R.color.secondaryBtn));
                    } else {
                        holder.displayImage.setBorderColor(ctx.getResources().getColor(R.color.colorPrimary));
                    }
                }
            });
        });

        holder.call.setOnClickListener(v -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("tel:"+data.get(position).getPhone()));
            ctx.startActivity(i);
        });

        holder.email.setOnClickListener(v1 -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("mailto:"+data.get(position).getEmail()));
            ctx.startActivity(i);
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


}
