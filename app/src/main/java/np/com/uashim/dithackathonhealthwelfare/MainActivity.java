package np.com.uashim.dithackathonhealthwelfare;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Document;

import np.com.uashim.dithackathonhealthwelfare.Interfaces.Hospital.HospitalDashboard;
import np.com.uashim.dithackathonhealthwelfare.Interfaces.Patient.PatientDashboard;
import np.com.uashim.dithackathonhealthwelfare.Model.User;

public class MainActivity extends AppCompatActivity {

    private Button patientLoginBtn, hospitalLoginBtn;
    private ProgressBar progressBar;
    private ConstraintLayout constraintLayout;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Firebase instance
        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        // Widget declaration
        constraintLayout = findViewById(R.id.theLogger);
        patientLoginBtn  = findViewById(R.id.login_as_patient);
        hospitalLoginBtn = findViewById(R.id.login_as_hospital);
        progressBar = findViewById(R.id.progress);

        // Initially
        constraintLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        // On PatientLoginPress
        patientLoginBtn.setOnClickListener(v -> {
            Intent i = new Intent(MainActivity.this, LoginPatient.class);
            startActivity(i);
        });

        // On HospitalLoginPress
        hospitalLoginBtn.setOnClickListener(v -> {
            Intent i = new Intent(MainActivity.this, LoginHospital.class);
            startActivity(i);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null) {
            DocumentReference documentReference = mFirestore.collection("users").document(currentUser.getUid());
            documentReference.get().addOnCompleteListener(task -> {
                if(task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if(documentSnapshot.exists()) {
                        User user = documentSnapshot.toObject(User.class);
                        if(user.getRole().equals("p")) {
                            Intent i = new Intent(MainActivity.this, PatientDashboard.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        } else {
                            Intent i = new Intent(MainActivity.this, HospitalDashboard.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        }
                    } else {
                        mAuth.signOut();
                    }
                } else {
                    mAuth.signOut();
                }
            });
        } else {
            constraintLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }
}
