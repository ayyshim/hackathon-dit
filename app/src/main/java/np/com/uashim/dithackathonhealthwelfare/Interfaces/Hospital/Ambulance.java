package np.com.uashim.dithackathonhealthwelfare.Interfaces.Hospital;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import np.com.uashim.dithackathonhealthwelfare.Adapter.AmbulanceListAdapter;
import np.com.uashim.dithackathonhealthwelfare.Model.AmbulanceModel;
import np.com.uashim.dithackathonhealthwelfare.R;

public class Ambulance extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AmbulanceListAdapter ambulanceListAdapter;
    private Button btnAdd;
    private List<AmbulanceModel> ambData = new ArrayList<>();
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance);

        // Firebase instnance
        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        initData();

        recyclerView = findViewById(R.id.ambList);

        btnAdd = findViewById(R.id.addAmbulance);

        btnAdd.setOnClickListener(v -> {
            LayoutInflater add_form = LayoutInflater.from(this);
            final View view = add_form.inflate(R.layout.add_ambulance, null);
            final AlertDialog v_dialog = new AlertDialog.Builder(this).create();
            v_dialog.setView(view);
            v_dialog.setTitle("Add an ambulance : ");

            EditText et_number = view.findViewById(R.id.et_number);
            Spinner spin_type = view.findViewById(R.id.typeSpin);
            Button addBtn = view.findViewById(R.id.addBtn);

            addBtn.setOnClickListener(v1 -> {
                String number = et_number.getText().toString();
                String type = spin_type.getSelectedItem().toString();
                Map<String, Object> ambulanceMap = new HashMap<>();
                ambulanceMap.put("hid", mAuth.getCurrentUser().getUid());
                ambulanceMap.put("ambulance_number", number);
                ambulanceMap.put("type", type);
                ambulanceMap.put("is_available", true);
                if(TextUtils.isEmpty(number)) {
                    return;
                }
                addBtn.setEnabled(false);
                v_dialog.setCancelable(false);
                mFirestore.collection("ambulances").add(ambulanceMap).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if(task.isSuccessful()) {
                            addBtn.setEnabled(true);
                            ambData.clear();
                            ambData = getData();
                            v_dialog.cancel();
                        } else {
                            addBtn.setEnabled(true);
                            v_dialog.setCancelable(true);
                            Toast.makeText(Ambulance.this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            });

            v_dialog.show();

        });




    }

    private void initData() {
        mFirestore.collection("ambulances").whereEqualTo("hid", mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(task -> {
            if(task.isSuccessful()) {
                QuerySnapshot doc = task.getResult();
                for(DocumentSnapshot documentSnapshot: doc) {
                    AmbulanceModel ambulanceModel = new AmbulanceModel(documentSnapshot.getId(), documentSnapshot.getString("type"), documentSnapshot.getString("ambulance_number"), documentSnapshot.getBoolean("is_available"));
                    ambData.add(ambulanceModel);
                }

                ambulanceListAdapter = new AmbulanceListAdapter(ambData, Ambulance.this, mFirestore);
                recyclerView.setAdapter(ambulanceListAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(Ambulance.this));

            }
        });
    }

    private List<AmbulanceModel> getData() {
        mFirestore.collection("ambulances").whereEqualTo("hid", mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(task -> {
            if(task.isSuccessful()) {
                QuerySnapshot doc = task.getResult();
                for(DocumentSnapshot documentSnapshot: doc) {
                    AmbulanceModel ambulanceModel = new AmbulanceModel(documentSnapshot.getId(),documentSnapshot.getString("type"), documentSnapshot.getString("ambulance_number"), documentSnapshot.getBoolean("is_available"));
                    ambData.add(ambulanceModel);
                }

                ambulanceListAdapter = new AmbulanceListAdapter(ambData, Ambulance.this, mFirestore);
                recyclerView.setAdapter(ambulanceListAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(Ambulance.this));

            }
        });

        return ambData;
    }

}
