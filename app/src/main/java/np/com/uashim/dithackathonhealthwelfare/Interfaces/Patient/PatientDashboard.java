package np.com.uashim.dithackathonhealthwelfare.Interfaces.Patient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import np.com.uashim.dithackathonhealthwelfare.MainActivity;
import np.com.uashim.dithackathonhealthwelfare.R;

public class PatientDashboard extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_dashboard);

        mAuth = FirebaseAuth.getInstance();

    }

    public void SignOut(View v) {
        mAuth.signOut();
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    public void Search(View v) {
        Intent i = new Intent(this, SearchHospital.class);
        startActivity(i);
    }

    public void Profile(View v) {
        Intent i = new Intent(this, PatientProfile.class);
        startActivity(i);
    }


}
